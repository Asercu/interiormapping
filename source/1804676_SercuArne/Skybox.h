# pragma once

#include "DXF.h"

using namespace DirectX;

//creates a mesh for an inverted cube, the normals point inwards so the cube is only visible from inside and not the outside
class Skybox : public BaseMesh
{
public:
	Skybox(ID3D11Device* device, ID3D11DeviceContext* deviceContext);
	~Skybox();

	virtual void sendData(ID3D11DeviceContext* device_context, D3D_PRIMITIVE_TOPOLOGY top = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

protected:
	void initBuffers(ID3D11Device* device);
};