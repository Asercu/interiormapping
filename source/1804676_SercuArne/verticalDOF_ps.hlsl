//very similar to the horizontal DOF

Texture2D shaderTexture : register(t0);
Texture2D depthMap : register(t1);
SamplerState SampleType : register(s0);

cbuffer ScreenSizeBuffer : register(b0)
{
    float screenWidth;
	float screenHeight;
	float blurIntensity;
	float dofSensitivity;
};

struct InputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
};

float4 main(InputType input) : SV_TARGET
{
	float4 colour;

	colour = shaderTexture.Sample(SampleType, input.tex);

	float4 blurredvalues[11];
	blurredvalues[0] = colour;

	float texelSize = 1.0f / screenHeight;
	for (int i = 1; i <= blurIntensity; i++)
	{
		blurredvalues[i] = blurredvalues[i - 1];
		blurredvalues[i] += shaderTexture.Sample(SampleType, input.tex + float2(0.0f, -texelSize * i));
		blurredvalues[i] += shaderTexture.Sample(SampleType, input.tex + float2(0.0f, texelSize * i));
	}
	float centerDepth = depthMap.Sample(SampleType, float2(0.5f, 0.5f)).x;
	float pixelDepth = depthMap.Sample(SampleType, input.tex).x;
	float depthDiff = abs(centerDepth - pixelDepth);
	if (depthDiff < dofSensitivity)
		return blurredvalues[0];

	int index = clamp(ceil(((depthDiff - dofSensitivity) / (1.0f - dofSensitivity)) * 2 * blurIntensity), 0, blurIntensity);
	colour = blurredvalues[index] / (2 * index + 1);
	colour.a = 1.f;
	return colour;
}
