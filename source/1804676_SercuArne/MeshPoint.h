#include "BaseMesh.h"

using namespace DirectX;

//mesh for sending a single point to the gpu
class MeshPoint : public BaseMesh
{
private:
	struct VertexType_Position
	{
		XMFLOAT3 position;
	};

public:
	MeshPoint(ID3D11Device* device, ID3D11DeviceContext* deviceContext, XMFLOAT3 position);
	~MeshPoint();

	void sendData(ID3D11DeviceContext*);

protected:
	void initBuffers(ID3D11Device* device);

private:
	XMFLOAT3 m_Position;
	ID3D11InputLayout* m_Layout;

};