//samples the cubemap and renders it on the far plane

// Texture and sampler registers
TextureCube texture0 : register(t0);
SamplerState Sampler0 : register(s0);

struct InputType
{
	float4 position : SV_POSITION;
	float3 tex : TEXCOORD0;
};


float4 main(InputType input) : SV_TARGET
{
	// Sample the pixel color from the texture using the sampler at this texture coordinate location.
    return texture0.Sample(Sampler0, input.tex);
}