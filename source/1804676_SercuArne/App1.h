// Application.h
#ifndef _APP1_H
#define _APP1_H

// Includes
#include "DXF.h"	// include dxframework
#include "TextureShader.h"
#include "InteriorMappingShader.h"
#include "MeshPoint.h"
#include "InteriorMappingGeometryShader.h"
#include "shadowshader.h"
#include "depthshader.h"
#include "Skybox.h"
#include "SkyBoxShader.h"
#include "DepthGeometryShader.h"
#include "DepthShaderPP.h"
#include "DepthGeometryShaderPP.h"
#include "HorizontalDOFShader.h"
#include "VerticalDOFShader.h"
#include "Quad.h"
#include "tessellationshader.h"
#include "TesselatedDepthShader.h"
#include "TesselatedDepthShaderPP.h"

class App1 : public BaseApplication
{
public:

	App1();
	~App1();
	void init(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight, Input* in, bool VSYNC, bool FULL_SCREEN);

	bool frame();

protected:
	bool render();
	void shadowDepthPass();
	void mainPass();
	void postProcessDepthPass();
	void verticalDOF();
	void horizontalDOF();
	void finalPass();
	void gui();

private:
	//shaders
	TextureShader* textureShader;
	InteriorMappingShader* interiorMappingShader;
	InteriorMappingGeometryShader* interiorMappingGeometryShader;
	ShadowShader* shadowShader;
	DepthShader* depthShader;
	DepthGeometryShader* depthGeometryShader;
	DepthShaderPP* depthShaderPP;
	DepthGeometryShaderPP* depthGeometryShaderPP;
	SkyBoxShader* skyboxShader;
	HorizontalDOFShader* horizontalDOFShader;
	VerticalDOFShader* verticalDOFShader;
	TessellationShader* tessellationShader;
	TessellatedDepthShader* tessellatedDepthShader;
	TessellatedDepthShaderPP* tessellatedDepthShaderPP;

	//scene
	CubeMesh* cubeBuilding;
	MeshPoint* pointBuilding;
	Skybox* sky;
	Light* light;
	OrthoMesh* orthoMesh;
	Quad* quad;

	//rendertargets
	RenderTexture* shadowMap;
	RenderTexture* depthMap;
	RenderTexture* verticalDOFTexture;
	RenderTexture* horizontalDOFTexture;
	RenderTexture* MainRenderTexture;

	//ImGUi variables
	int numFloors = 1;
	int numRoomsX = 1;
	int numRoomsZ = 1;
	float buildingHeight = 1;
	float buildingWidth = 1;
	float buildingDepth = 1;
	int tesFactor = 64;
	float maxheight = 30.f;
	float DOFSensitivity = 0.1f;
	int blurIntensity = 0;
	bool usePointMesh = false;
};

#endif