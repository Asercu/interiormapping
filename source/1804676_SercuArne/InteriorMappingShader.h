#pragma once
#include "include/BaseShader.h"


class Light;

//this shader will apply interior mapping on a given object
class InteriorMappingShader : public BaseShader
{
protected:
	struct MatrixBufferType
	{
		XMMATRIX world;
		XMMATRIX view;
		XMMATRIX projection;
		XMMATRIX lightView;
		XMMATRIX lightProjection;
	};

	struct InteriorBufferType
	{
		XMFLOAT3 cameraPos;
		float padding;
	};

	struct BuildingBufferType
	{
		int numFloors;
		int numRoomsX;
		int numRoomsZ;
		int paddingA;
		float buildingHeight;
		float buildingWidth;
		float buildingDepth;
		float paddingB;
	};

	struct LightBufferType
	{
		XMFLOAT4 lightColor;
		XMFLOAT4 diffuseColor;
		XMFLOAT3 lightDirection;
		float specularPower;
		XMFLOAT4 specularColour;
	};

public:
	InteriorMappingShader(ID3D11Device* device, HWND hwnd);
	~InteriorMappingShader();

	virtual void setShaderParameters(
		ID3D11DeviceContext* deviceContext,
		const XMMATRIX &world,
		const XMMATRIX &view,
		const XMMATRIX &projection,
		const XMFLOAT3 cameraPos,
		const int numFloors,
		const int numRoomsX,
		const int numRoomsZ,
		const float buildingHeight,
		const float buildingWidth,
		const float buildingDepth,
		Light* light,
		ID3D11ShaderResourceView* depthMap
	);

	void SetWallTexture(ID3D11ShaderResourceView* tex) { wallTexture = tex; };
	void SetFloorTexture(ID3D11ShaderResourceView* tex) { floorTexture = tex; };
	void SetCeilingTexture(ID3D11ShaderResourceView* tex) { ceilingTexture = tex; };
	void SetExteriorTexture(ID3D11ShaderResourceView* tex) { exteriorTexture = tex; };
	void SetEnvironmentMap(ID3D11ShaderResourceView* tex) { environmentCubeMap = tex; };

protected:
	void initShader(WCHAR*, WCHAR*);

	ID3D11Buffer* matrixBuffer;
	ID3D11Buffer* interiorBuffer;
	ID3D11Buffer* buildingBuffer;
	ID3D11Buffer* lightBuffer;
	ID3D11SamplerState* sampleState;
	ID3D11SamplerState* exteriorSampleState;
	ID3D11SamplerState* sampleStateShadow;

	ID3D11ShaderResourceView* ceilingTexture;
	ID3D11ShaderResourceView* floorTexture;
	ID3D11ShaderResourceView* wallTexture;
	ID3D11ShaderResourceView* exteriorTexture;
	ID3D11ShaderResourceView* environmentCubeMap;
};

