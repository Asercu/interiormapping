#include "InteriorMappingGeometryShader.h"
#include "Light.h"


InteriorMappingGeometryShader::InteriorMappingGeometryShader(ID3D11Device* device, HWND hwnd) : InteriorMappingShader(device, hwnd)
{
	initShader(L"interiorMappingGeometry_vs.cso", L"interiorMapping_gs.cso", L"interiorMapping_ps.cso");
}


InteriorMappingGeometryShader::~InteriorMappingGeometryShader()
{
	InteriorMappingShader::~InteriorMappingShader();
}

void InteriorMappingGeometryShader::initShader(WCHAR* vsFilename, WCHAR* gsFilename, WCHAR* psFilename)
{
	InteriorMappingShader::initShader(vsFilename, psFilename);
	loadGeometryShader(gsFilename);
}

void InteriorMappingGeometryShader::setShaderParameters(
	ID3D11DeviceContext* deviceContext,
	const XMMATRIX &worldMatrix,
	const XMMATRIX &viewMatrix,
	const XMMATRIX &projectionMatrix,
	const XMFLOAT3 cameraPos,
	const int numFloors,
	const int numRoomsX,
	const int numRoomsZ,
	const float buildingHeight,
	const float buildingWidth,
	const float buildingDepth,
	Light* light,
	ID3D11ShaderResourceView* depthMap)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	MatrixBufferType* dataPtr;
	InteriorBufferType* interiorPtr;
	BuildingBufferType* buildingPtr;
	LightBufferType* lightPtr;
	XMMATRIX tworld, tview, tproj;


	// Transpose the matrices to prepare them for the shader.
	tworld = XMMatrixTranspose(worldMatrix);
	tview = XMMatrixTranspose(viewMatrix);
	tproj = XMMatrixTranspose(projectionMatrix);
	XMMATRIX tLightViewMatrix = XMMatrixTranspose(light->getViewMatrix());
	XMMATRIX tLightProjectionMatrix = XMMatrixTranspose(light->getOrthoMatrix());

	// Send matrix data
	result = deviceContext->Map(matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	dataPtr = (MatrixBufferType*)mappedResource.pData;
	dataPtr->world = tworld;// worldMatrix;
	dataPtr->view = tview;
	dataPtr->projection = tproj;
	dataPtr->lightView = tLightViewMatrix;
	dataPtr->lightProjection = tLightProjectionMatrix;
	deviceContext->Unmap(matrixBuffer, 0);
	deviceContext->VSSetConstantBuffers(0, 1, &matrixBuffer);
	deviceContext->GSSetConstantBuffers(0, 1, &matrixBuffer);

	// send the interior data
	result = deviceContext->Map(interiorBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	interiorPtr = (InteriorBufferType*)mappedResource.pData;
	interiorPtr->cameraPos = cameraPos;
	interiorPtr->padding = 0.f;
	deviceContext->Unmap(interiorBuffer, 0);
	deviceContext->PSSetConstantBuffers(0, 1, &interiorBuffer);

	// send the building data
	result = deviceContext->Map(buildingBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	buildingPtr = (BuildingBufferType*)mappedResource.pData;
	buildingPtr->numFloors = numFloors;
	buildingPtr->numRoomsX = numRoomsX;
	buildingPtr->numRoomsZ = numRoomsZ;
	buildingPtr->paddingA = 0;
	buildingPtr->buildingHeight = buildingHeight;
	buildingPtr->buildingWidth = buildingWidth;
	buildingPtr->buildingDepth = buildingDepth;
	buildingPtr->paddingB = 0.f;
	deviceContext->Unmap(buildingBuffer, 0);
	deviceContext->GSSetConstantBuffers(1, 1, &buildingBuffer);
	deviceContext->PSSetConstantBuffers(1, 1, &buildingBuffer);

	//send light data
	result = deviceContext->Map(lightBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	lightPtr = (LightBufferType*)mappedResource.pData;
	lightPtr->diffuseColor = light->getDiffuseColour();
	lightPtr->lightColor = light->getAmbientColour();
	lightPtr->lightDirection = light->getDirection();
	lightPtr->specularColour = light->getSpecularColour();
	lightPtr->specularPower = light->getSpecularPower();
	deviceContext->Unmap(lightBuffer, 0);
	deviceContext->PSSetConstantBuffers(2, 1, &lightBuffer);

	// Set shader texture and sampler resource in the pixel shader.
	deviceContext->PSSetShaderResources(0, 1, &ceilingTexture);
	deviceContext->PSSetShaderResources(1, 1, &floorTexture);
	deviceContext->PSSetShaderResources(2, 1, &wallTexture);
	deviceContext->PSSetShaderResources(3, 1, &exteriorTexture);
	deviceContext->PSSetShaderResources(4, 1, &environmentCubeMap);
	deviceContext->PSSetShaderResources(5, 1, &depthMap);
	deviceContext->PSSetSamplers(0, 1, &sampleState);
	deviceContext->PSSetSamplers(1, 1, &exteriorSampleState);
	deviceContext->PSSetSamplers(2, 1, &sampleStateShadow);
}
