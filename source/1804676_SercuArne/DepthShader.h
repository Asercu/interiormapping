#pragma once

#include "DXF.h"

using namespace std;
using namespace DirectX;

//renders out a depth map
class DepthShader : public BaseShader
{

public:

	DepthShader(ID3D11Device* device, HWND hwnd);
	~DepthShader();

	void setShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection);

protected:
	void initShader(WCHAR*, WCHAR*);

	ID3D11Buffer* matrixBuffer;
};
