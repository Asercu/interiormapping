//generates the cube for the building based in its properties in the buildingbuffer

cbuffer MatrixBuffer : register(b0)
{
    matrix worldMatrix;
    matrix viewMatrix;
    matrix projectionMatrix;
	matrix lightViewMatrix;
	matrix lightProjectionMatrix;
};

cbuffer BuildingBuffer : register(b1)
{
	int numFloors;
	int numRoomsX;
	int numRoomsZ;
	int paddingA;
	float buildingHeight;
	float buildingWidth;
	float buildingDepth;
	float paddingB;
}

struct InputType
{
	float4 position : POSITION;
};

struct OutputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float4 worldPosition : TEXCOORD1;
	float4 lightViewPos : TEXCOORD2;
};

OutputType bottomLeftFront(InputType input, float3 normal, float2 tex)
{
	OutputType output;
	output.worldPosition = float4(input.position.x + 0.005f, input.position.y + 0.005f, input.position.z + 0.005f, input.position.w);

	output.lightViewPos = mul(output.worldPosition, worldMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightViewMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightProjectionMatrix);

	output.position = mul(output.worldPosition, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);
	output.tex = tex;
	output.normal = normal;
	return output;
}

OutputType bottomRightFront(InputType input, float3 normal, float2 tex)
{
	OutputType output;
	output.worldPosition = float4(input.position.x + 2.0f * buildingWidth * 0.99f, input.position.y + 0.005f, input.position.z + 0.005f, input.position.w);
	
	output.lightViewPos = mul(output.worldPosition, worldMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightViewMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightProjectionMatrix);
	
	output.position = mul(output.worldPosition, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);
	output.tex = tex;
	output.normal = normal;
	return output;
}

OutputType topRightFront(InputType input, float3 normal, float2 tex)
{
	OutputType output;
	output.worldPosition = float4(input.position.x + 2.0f * buildingWidth * 0.99f, input.position.y + 2.0f * buildingHeight * 0.99f, input.position.z + 0.005f, input.position.w);
	
	output.lightViewPos = mul(output.worldPosition, worldMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightViewMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightProjectionMatrix);
	
	output.position = mul(output.worldPosition, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);
	output.tex = tex;
	output.normal = normal;
	return output;
}

OutputType topLeftFront(InputType input, float3 normal, float2 tex)
{
	OutputType output;
	output.worldPosition = float4(input.position.x + 0.0005f, input.position.y + 2.0f * buildingHeight * 0.99f, input.position.z + 0.005f, input.position.w);
	
	output.lightViewPos = mul(output.worldPosition, worldMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightViewMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightProjectionMatrix);
	
	output.position = mul(output.worldPosition, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);
	output.tex = tex;
	output.normal = normal;
	return output;
}

OutputType bottomLeftBack(InputType input, float3 normal, float2 tex)
{
	OutputType output;
	output.worldPosition = float4(input.position.x + 0.005f, input.position.y + 0.005f, input.position.z + 2.0f * buildingDepth * 0.99f, input.position.w);
	
	output.lightViewPos = mul(output.worldPosition, worldMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightViewMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightProjectionMatrix);
	
	output.position = mul(output.worldPosition, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);
	output.tex = tex;
	output.normal = normal;
	return output;
}

OutputType bottomRightBack(InputType input, float3 normal, float2 tex)
{
	OutputType output;
	output.worldPosition = float4(input.position.x + 2.0f * buildingWidth * 0.99f, input.position.y + 0.005f, input.position.z + 2.0f * buildingDepth * 0.99f, input.position.w);
	
	output.lightViewPos = mul(output.worldPosition, worldMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightViewMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightProjectionMatrix);
	
	output.position = mul(output.worldPosition, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);
	output.tex = tex;
	output.normal = normal;
	return output;
}

OutputType topRightBack(InputType input, float3 normal, float2 tex)
{
	OutputType output;
	output.worldPosition = float4(input.position.x + 2.0f * buildingWidth * 0.99f, input.position.y + 2.0f * buildingHeight * 0.99f, input.position.z + 2.0f * buildingDepth * 0.99f, input.position.w);
	
	output.lightViewPos = mul(output.worldPosition, worldMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightViewMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightProjectionMatrix);
	
	output.position = mul(output.worldPosition, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);
	output.tex = tex;
	output.normal = normal;
	return output;
}

OutputType topLeftBack(InputType input, float3 normal, float2 tex)
{
	OutputType output;
	output.worldPosition = float4(input.position.x + 0.005f, input.position.y + 2.0f * buildingHeight * 0.99f, input.position.z + 2.0f * buildingDepth * 0.99f, input.position.w);
	
	output.lightViewPos = mul(output.worldPosition, worldMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightViewMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightProjectionMatrix);
	
	output.position = mul(output.worldPosition, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);
	output.tex = tex;
	output.normal = normal;
	return output;
}

[maxvertexcount(36)]
void main(point InputType input[1], inout TriangleStream<OutputType> triStream)
{
	OutputType output;
	//front
	triStream.Append(bottomLeftFront(input[0], float3(0, 0, -1), float2(0, numFloors)));
	triStream.Append(topRightFront(input[0], float3(0, 0, -1), float2(numRoomsX, 0)));
	triStream.Append(topLeftFront(input[0], float3(0, 0, -1), float2(0, 0)));
	triStream.RestartStrip();
	triStream.Append(bottomLeftFront(input[0], float3(0, 0, -1), float2(0, numFloors)));
	triStream.Append(bottomRightFront(input[0], float3(0, 0, -1), float2(numRoomsX, numFloors)));
	triStream.Append(topRightFront(input[0], float3(0, 0, -1), float2(numRoomsX, 0)));
	triStream.RestartStrip();
	//right
	triStream.Append(bottomRightFront(input[0], float3(1, 0, 0), float2(0, numFloors)));
	triStream.Append(bottomRightBack(input[0], float3(1, 0, 0), float2(numRoomsZ, numFloors)));
	triStream.Append(topRightBack(input[0], float3(1, 0, 0), float2(numRoomsZ, 0)));
	triStream.RestartStrip();
	triStream.Append(bottomRightFront(input[0], float3(1, 0, 0), float2(0, numFloors)));
	triStream.Append(topRightBack(input[0], float3(1, 0, 0), float2(numRoomsZ, 0)));
	triStream.Append(topRightFront(input[0], float3(1, 0, 0), float2(0, 0)));
	triStream.RestartStrip();
	//back
	triStream.Append(bottomLeftBack(input[0], float3(0, 0, 1), float2(numRoomsX, numFloors)));
	triStream.Append(topLeftBack(input[0], float3(0, 0, 1), float2(numRoomsX, 0)));
	triStream.Append(bottomRightBack(input[0], float3(0, 0, 1), float2(0, numFloors)));
	triStream.RestartStrip();
	triStream.Append(topLeftBack(input[0], float3(0, 0, 1), float2(numRoomsX, 0)));
	triStream.Append(topRightBack(input[0], float3(0, 0, 1), float2(0, 0)));
	triStream.Append(bottomRightBack(input[0], float3(0, 0, 1), float2(0, numFloors)));
	triStream.RestartStrip();
	//left
	triStream.Append(bottomLeftFront(input[0], float3(-1, 0, 0), float2(numRoomsZ, numFloors)));
	triStream.Append(topLeftBack(input[0], float3(-1, 0, 0), float2(0, 0)));
	triStream.Append(bottomLeftBack(input[0], float3(-1, 0, 0), float2(0, numFloors)));
	triStream.RestartStrip();
	triStream.Append(bottomLeftFront(input[0], float3(-1, 0, 0), float2(numRoomsZ, numFloors)));
	triStream.Append(topLeftFront(input[0], float3(-1, 0, 0), float2(numRoomsZ, 0)));
	triStream.Append(topLeftBack(input[0], float3(-1, 0, 0), float2(0, 0)));
	triStream.RestartStrip();
	//top
	triStream.Append(topRightFront(input[0], float3(0, 1, 0), float2(0, 0)));
	triStream.Append(topLeftBack(input[0], float3(0, 1, 0), float2(0, 0)));
	triStream.Append(topLeftFront(input[0], float3(0, 1, 0), float2(0, 0)));
	triStream.RestartStrip();
	triStream.Append(topRightFront(input[0], float3(0, 1, 0), float2(0, 0)));
	triStream.Append(topRightBack(input[0], float3(0, 1, 0), float2(0, 0)));
	triStream.Append(topLeftBack(input[0], float3(0, 1, 0), float2(0, 0)));
	triStream.RestartStrip();
	//bottom
	triStream.Append(bottomRightFront(input[0], float3(0, -1, 0), float2(0, 0)));
	triStream.Append(bottomLeftFront(input[0], float3(0, -1, 0), float2(0, 0)));
	triStream.Append(bottomLeftBack(input[0], float3(0, -1, 0), float2(0, 0)));
	triStream.RestartStrip();
	triStream.Append(bottomRightFront(input[0], float3(0, -1, 0), float2(0, 0)));
	triStream.Append(bottomLeftBack(input[0], float3(0, -1, 0), float2(0, 0)));
	triStream.Append(bottomRightBack(input[0], float3(0, -1, 0), float2(0, 0)));
	triStream.RestartStrip();
}