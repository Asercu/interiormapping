// depth shader.cpp
#include "DepthGeometryShaderPP.h"

DepthGeometryShaderPP::DepthGeometryShaderPP(ID3D11Device* device, HWND hwnd) : DepthShader(device, hwnd)
{
	initShader(L"interiorMappingGeometry_vs.cso", L"depth_gs.cso", L"depthPP_ps.cso");
}

DepthGeometryShaderPP::~DepthGeometryShaderPP()
{
	if (buildingBuffer)
	{
		buildingBuffer->Release();
		buildingBuffer = 0;
	}
}

void DepthGeometryShaderPP::setShaderParameters(
	ID3D11DeviceContext* deviceContext,
	const XMMATRIX& world,
	const XMMATRIX& view, 
	const XMMATRIX& projection,
	const int numFloors,
	const int numRoomsX,
	const int numRoomsZ,
	const float buildingHeight,
	const float buildingWidth,
	const float buildingDepth)
{
	DepthShader::setShaderParameters(deviceContext, world, view, projection);
	deviceContext->GSSetConstantBuffers(0, 1, &matrixBuffer);

	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	BuildingBufferType* buildingPtr;
	// send the building data
	result = deviceContext->Map(buildingBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	buildingPtr = (BuildingBufferType*)mappedResource.pData;
	buildingPtr->numFloors = numFloors;
	buildingPtr->numRoomsX = numRoomsX;
	buildingPtr->numRoomsZ = numRoomsZ;
	buildingPtr->paddingA = 0;
	buildingPtr->buildingHeight = buildingHeight;
	buildingPtr->buildingWidth = buildingWidth;
	buildingPtr->buildingDepth = buildingDepth;
	buildingPtr->paddingB = 0.f;
	deviceContext->Unmap(buildingBuffer, 0);
	deviceContext->VSSetConstantBuffers(1, 1, &buildingBuffer);

}

void DepthGeometryShaderPP::initShader(WCHAR* vsFilename, WCHAR* gsFilename, WCHAR* psFilename)
{
	DepthShader::initShader(vsFilename, psFilename);
	loadGeometryShader(gsFilename);

	D3D11_BUFFER_DESC buildingBufferDesc;

	// Setup the description of the interior constant buffer that is in the vertex and pixel shaders.
	buildingBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	buildingBufferDesc.ByteWidth = sizeof(BuildingBufferType);
	buildingBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	buildingBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	buildingBufferDesc.MiscFlags = 0;
	buildingBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	renderer->CreateBuffer(&buildingBufferDesc, NULL, &buildingBuffer);
}
