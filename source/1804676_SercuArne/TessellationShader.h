#pragma once

#include "DXF.h"

using namespace std;
using namespace DirectX;

//shader for tesselation and heightmap
class TessellationShader : public BaseShader
{
private:
	struct MatrixBufferType
	{
		XMMATRIX world;
		XMMATRIX view;
		XMMATRIX projection;
		XMMATRIX lightView;
		XMMATRIX lightProjection;
	};

	struct LightBufferType
	{
		XMFLOAT4 ambient;
		XMFLOAT4 diffuse;
		XMFLOAT3 direction;
		float padding;
	};

	struct TessellationBufferType
	{
		int tesselationFactor;
		float height;
		XMFLOAT2 padding;
	};


public:

	TessellationShader(ID3D11Device* device, HWND hwnd);
	~TessellationShader();

	void setShaderParameters(
		ID3D11DeviceContext* deviceContext,
		const XMMATRIX &worldMatrix,
		const XMMATRIX &viewMatrix,
		const XMMATRIX &projectionMatrix,
		ID3D11ShaderResourceView* heightMap,
		ID3D11ShaderResourceView* grassTexture,
		ID3D11ShaderResourceView* rockTexture,
		ID3D11ShaderResourceView* shadowMap,
		Light* light,
		float maxHeight,
		int tesselationFactor);

private:
	void initShader(WCHAR* vsFilename, WCHAR* psFilename);
	void initShader(WCHAR* vsFilename, WCHAR* hsFilename, WCHAR* dsFilename, WCHAR* psFilename);

private:
	ID3D11Buffer* matrixBuffer;
	ID3D11SamplerState* sampleState;
	ID3D11SamplerState* sampleStateShadow;
	ID3D11Buffer* lightBuffer;
	ID3D11Buffer* tessellationBuffer;
};
