#pragma once
#include "DepthShader.h"

//renders out the depthmap of the building using the geometry shader
class DepthGeometryShader : public DepthShader
{
private:
	struct BuildingBufferType
	{
		int numFloors;
		int numRoomsX;
		int numRoomsZ;
		int paddingA;
		float buildingHeight;
		float buildingWidth;
		float buildingDepth;
		float paddingB;
	};

public:
	DepthGeometryShader(ID3D11Device* device, HWND hwnd);
	~DepthGeometryShader();

	void setShaderParameters(
		ID3D11DeviceContext* deviceContext,
		const XMMATRIX& world,
		const XMMATRIX& view,
		const XMMATRIX& projection,
		const int numFloors,
		const int numRoomsX,
		const int numRoomsZ,
		const float buildingHeight,
		const float buildingWidth,
		const float buildingDepth);
private:
	void initShader(WCHAR*, WCHAR*, WCHAR*);

private:
	ID3D11Buffer* buildingBuffer;
};
