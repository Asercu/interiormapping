//moves the vertex to the corresponding place on the far plane to render a skybox

cbuffer MatrixBuffer : register(b0)
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

struct InputType
{
	float4 position : POSITION;
	float2 tex : TEXCOORD0;
    float3 normal : NORMAL;
};

struct OutputType
{
	float4 position : SV_POSITION;
	float3 tex : TEXCOORD0;
};

OutputType main(InputType input)
{
	OutputType output;

	// Calculate the position of the vertex against the world, view, and projection matrices.
	output.position = mul(input.position, worldMatrix);
	output.position = mul(output.position, viewMatrix);
	//bu sampling xyww we place the pixel on the far plane;
	output.position = mul(output.position, projectionMatrix).xyww;

	// Store the texture coordinates for the pixel shader.
	output.tex = input.position.xyz;

	return output;
}