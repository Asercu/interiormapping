#pragma once

#include "BaseShader.h"

using namespace std;
using namespace DirectX;

//shader for rendering the object on the far plane
class SkyBoxShader : public BaseShader
{
public:
	SkyBoxShader(ID3D11Device* device, HWND hwnd);
	~SkyBoxShader();

	void setShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection, ID3D11ShaderResourceView* texture);

private:
	void initShader(WCHAR*, WCHAR*);

	ID3D11Buffer *matrixBuffer;
	ID3D11SamplerState* sampleState;
};