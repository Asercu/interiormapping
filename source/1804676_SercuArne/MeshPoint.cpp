#include "MeshPoint.h"

MeshPoint::MeshPoint(ID3D11Device * device, ID3D11DeviceContext * deviceContext, XMFLOAT3 position)
{
	m_Position = position;
	initBuffers(device);
}

MeshPoint::~MeshPoint()
{
	BaseMesh::~BaseMesh();
}

void MeshPoint::initBuffers(ID3D11Device * device)
{
	/*D3D11_INPUT_ELEMENT_DESC layoutDesc[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	device->CreateInputLayout(layoutDesc, 1, , , &m_Layout);*/

	D3D11_SUBRESOURCE_DATA vertexData, indexData;

	vertexCount = 1;
	indexCount = 1;

	VertexType* vertices = new VertexType[vertexCount];
	unsigned long* indices = new unsigned long[indexCount];

	vertices[0].position = m_Position;
	vertices[0].texture = XMFLOAT2(0, 0);
	vertices[0].normal = XMFLOAT3(0, 0, 0);

	indices[0] = 0;

	D3D11_BUFFER_DESC vertexBufferDesc = { sizeof(VertexType) * vertexCount, D3D11_USAGE_DEFAULT, D3D11_BIND_VERTEX_BUFFER, 0, 0, 0 };
	vertexData = { vertices, 0, 0 };
	device->CreateBuffer(&vertexBufferDesc, &vertexData, &vertexBuffer);

	D3D11_BUFFER_DESC indexBufferDesc = { sizeof(unsigned long) * indexCount, D3D11_USAGE_DEFAULT, D3D11_BIND_INDEX_BUFFER, 0, 0, 0 };
	indexData = { indices, 0, 0 };
	device->CreateBuffer(&indexBufferDesc, &indexData, &indexBuffer);

	delete[] vertices;
	vertices = 0;
	delete[] indices;
	indices = 0;
}

void MeshPoint::sendData(ID3D11DeviceContext * deviceContext)
{
	unsigned int stride;
	unsigned int offset;

	stride = sizeof(VertexType);
	offset = 0;

	//deviceContext->IASetInputLayout(m_Layout);
	deviceContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	deviceContext->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
}
