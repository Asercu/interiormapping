#pragma once
#include "InteriorMappingShader.h"

//this shader creates a building entirely on the GPU based on the building dimensions and a position in the world
class InteriorMappingGeometryShader : public InteriorMappingShader
{
public:
	InteriorMappingGeometryShader(ID3D11Device* device, HWND hwnd);
	~InteriorMappingGeometryShader();

	void setShaderParameters(
		ID3D11DeviceContext* deviceContext,
		const XMMATRIX &world,
		const XMMATRIX &view,
		const XMMATRIX &projection,
		const XMFLOAT3 cameraPos,
		const int numFloors,
		const int numRoomsX,
		const int numRoomsZ,
		const float buildingHeight,
		const float buildingWidth,
		const float buildingDepth,
		Light* light,
		ID3D11ShaderResourceView* depthmap
	);

private:
	void initShader(WCHAR* vsFilename, WCHAR* gsFilename, WCHAR* psFilename);
};

