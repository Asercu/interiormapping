//main part of interior mapping, will render the outside walls, and the mapped interior in the place of windows with environmental reflections from a cubemap

Texture2D ceiling : register(t0);
Texture2D floor : register(t1);
Texture2D wall : register(t2);
Texture2D exterior : register(t3);
TextureCube environment : register(t4);
Texture2D depthMapTexture : register(t5);
SamplerState Sampler0 : register(s0);
SamplerState exteriorSampler : register(s1);
SamplerState shadowSampler : register(s2);

cbuffer interiorBuffer : register(b0)
{
	float3 cameraPos;
	float interiorPadding;
}

cbuffer BuildingBuffer : register(b1)
{
	int numFloors;
	int numRoomsX;
	int numRoomsZ;
	int paddingA;
	float buildingHeight;
	float buildingWidth;
	float buildingDepth;
	float paddingB;
}

cbuffer LightBuffer : register(b2)
{
	float4 lightColor;
	float4 diffuseColor;
	float3 lightDirection;
	float specularPower;
	float4 specularColor;
}

struct InputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
    float3 normal : NORMAL;
	float4 worldPosition : TEXCOORD1;
	float4 lightViewPos : TEXCOORD2;
};

float4 calculateLighting(float3 lightDirection, float3 normal, float4 diffuse)
{
	float intensity = saturate(dot(normal, lightDirection));
	float4 colour = saturate(diffuse * intensity);
	return colour;
}

float4 main(InputType input) : SV_TARGET
{
	float depthValue;
	float lightDepthValue;
	float shadowMapBias = 0.005f;
	float4 colour = float4(0.f, 0.f, 0.f, 1.f);
	float4 textureColor;

	//sample the color of the exterior textures
	float4 exteriorColor = exterior.Sample(exteriorSampler, input.tex);
	//widows will have an alpha value of 0
	//sets textureColor
	if (exteriorColor.a != 0)
	{
		//no window, use the texture color
		textureColor = exteriorColor;
	}
	else
	{
		//used for aligning the textures. if a texture would contain more walls than just the one these values can be changed to make them line up
		float roomHeight = 2;
		float roomDepth = 2;
		float roomWidth = 2;
		//get the viewvector
		float3 direction = input.worldPosition.xyz - cameraPos;
		direction = normalize(direction);

		//calculate intersections
		float height = (ceil(input.worldPosition.y / ((roomHeight * buildingHeight) / numFloors)) - (direction.y < 0)) * ((roomHeight * buildingHeight) / numFloors);
		float rayFractionXZ = (height - cameraPos.y) / direction.y;
		float depth = (ceil(input.worldPosition.z / ((roomDepth * buildingDepth) / numRoomsZ)) - (direction.z < 0)) * ((roomDepth * buildingDepth) / numRoomsZ);
		float rayFractionXY = (depth - cameraPos.z) / direction.z;
		float width = (ceil(input.worldPosition.x / ((roomWidth * buildingWidth) / numRoomsX)) - (direction.x < 0)) * ((roomWidth * buildingWidth) / numRoomsX);
		float rayFractionZY = (width - cameraPos.x) / direction.x;

		//get smallest fraction
		if (rayFractionXZ < rayFractionXY && rayFractionXZ < rayFractionZY)
		{
			float2 intersection = 1 * (cameraPos + rayFractionXZ * direction).xz;
			//ceiling/floor
			if (direction.y > 0)
			{
				textureColor = ceiling.Sample(Sampler0, float2(intersection.x * (numRoomsX / buildingWidth), intersection.y * (numRoomsZ / buildingDepth)));
			}
			else
			{
				textureColor = floor.Sample(Sampler0, float2(intersection.x * (numRoomsX / buildingWidth), intersection.y * (numRoomsZ / buildingDepth)));
			}
		}
		//walls
		else if (rayFractionXY < rayFractionXZ && rayFractionXY < rayFractionZY)
		{
			//depth
			float2 intersection = .5 * (cameraPos + rayFractionXY * direction).xy;
			textureColor = wall.Sample(Sampler0, float2(intersection.x * (numRoomsX / buildingWidth), intersection.y * (numFloors / buildingHeight)));
		}
		else
		{
			//width
			float2 intersection = .5 * (cameraPos + rayFractionZY * direction).yz;
			textureColor = wall.Sample(Sampler0, float2(intersection.y * (numRoomsZ / buildingDepth), intersection.x * (numFloors / buildingHeight)));
		}
		//environment reflection
		float3 reflection = reflect(direction, input.normal);
		float4 reflectionColor = environment.Sample(Sampler0, reflection);

		//combine both values
		textureColor = 0.8f * textureColor + 0.2 * reflectionColor;
	}

	float2 pTexCoord = input.lightViewPos.xy / input.lightViewPos.w;
	pTexCoord *= float2(0.5, -0.5);
	pTexCoord += float2(0.5f, 0.5f);

	if (pTexCoord.x < 0.f || pTexCoord.x > 1.f || pTexCoord.y < 0.f || pTexCoord.y > 1.f)
	{
		return textureColor;
	}

	depthValue = depthMapTexture.Sample(shadowSampler, pTexCoord).r;

	lightDepthValue = input.lightViewPos.z / input.lightViewPos.w;
	lightDepthValue -= shadowMapBias;

	if (lightDepthValue < depthValue)
	{
		colour = calculateLighting(-lightDirection, input.normal, diffuseColor);
	}

	colour = saturate(colour + lightColor);

	return colour * textureColor;

	/*
	//apply directional light
	float4 lighting = lightColor;
	lighting += calculateLighting(-lightDirection, input.normal, diffuseColor);

	return textureColor * lighting;*/
}