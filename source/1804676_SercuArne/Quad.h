#pragma once
#include "include/BaseMesh.h"
#include "DXF.h"

using namespace DirectX;

class Quad :
	public BaseMesh
{
public:
	Quad(ID3D11Device* device, ID3D11DeviceContext* deviceContext);
	~Quad();

	virtual void sendData(ID3D11DeviceContext* deviceContext, D3D_PRIMITIVE_TOPOLOGY top);
protected:
	void initBuffers(ID3D11Device* device);
};

