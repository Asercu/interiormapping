#include "Skybox.h"

Skybox::Skybox(ID3D11Device* device, ID3D11DeviceContext* deviceContext)
{
	initBuffers(device);
}

Skybox::~Skybox()
{
	BaseMesh::~BaseMesh();
}

void Skybox::initBuffers(ID3D11Device* device)
{
	D3D11_SUBRESOURCE_DATA vertexData, indexData;

	vertexCount = 16;
	indexCount = 36;

	VertexType_Texture* vertices = new VertexType_Texture[vertexCount];
	unsigned long* indices = new unsigned long[indexCount];


	// Load the vertex array with data.
	vertices[0].position = XMFLOAT3(-1.0f, 1.0f, 1.0f);
	vertices[0].texture = XMFLOAT2(0.34f, 0.34f);

	vertices[1].position = XMFLOAT3(1.0f, 1.0f, 1.0f); 
	vertices[1].texture = XMFLOAT2(0.65f, 0.34f);

	vertices[2].position = XMFLOAT3(-1.f, -1.f, 1.f);
	vertices[2].texture = XMFLOAT2(0.34f, 0.65f);

	vertices[3].position = XMFLOAT3(1.f, -1.f, 1.f);
	vertices[3].texture = XMFLOAT2(0.65f, 0.65f);

	vertices[4].position = XMFLOAT3(-1.f, 1.f, -1.f);
	vertices[4].texture = XMFLOAT2(0.f, 0.34f);

	vertices[5].position = XMFLOAT3(-1.f, -1.f, -1.f);
	vertices[5].texture = XMFLOAT2(0.f, 0.65f);

	vertices[6].position = XMFLOAT3(-1.f, -1.f, -1.f);
	vertices[6].texture = XMFLOAT2(0.35f, 1.f);

	vertices[7].position = XMFLOAT3(1.f, -1.f, -1.f);
	vertices[7].texture = XMFLOAT2(0.65f, 1.f);

	vertices[8].position = XMFLOAT3(1.f, -1.f, -1.f);
	vertices[8].texture = XMFLOAT2(1.f, 0.65f);

	vertices[9].position = XMFLOAT3(1.f, 1.f, -1.f);
	vertices[9].texture = XMFLOAT2(1.f, 0.34f);

	vertices[10].position = XMFLOAT3(1.f, 1.f, -1.f);
	vertices[10].texture = XMFLOAT2(0.65f, 0.f);

	vertices[11].position = XMFLOAT3(-1.f, 1.f, -1.f);
	vertices[11].texture = XMFLOAT2(0.34f, 0.f);

	vertices[12].position = XMFLOAT3(-1.f, -1.f, -1.f);
	vertices[12].texture = XMFLOAT2(1.f, 1.f);

	vertices[13].position = XMFLOAT3(-1.f, 1.f, -1.f);
	vertices[13].texture = XMFLOAT2(1.f, 0.67f);

	vertices[14].position = XMFLOAT3(1.f, 1.f, -1.f);
	vertices[14].texture = XMFLOAT2(0.67f, 0.67f);

	vertices[15].position = XMFLOAT3(1.f, -1.f, -1.f);
	vertices[15].texture = XMFLOAT2(0.67f, 1.f);
		
	

	// Load the index array with data.
	//back plane
	indices[0] = 0;  // back Top left
	indices[1] = 2;  // back Bottom left.
	indices[2] = 3;  // back Bottom right.
	indices[3] = 0;  // back Top left.
	indices[4] = 3;  // back Bottom right.
	indices[5] = 1;  // back Top right.
	//front plane
	indices[6] = 14;
	indices[7] = 12;
	indices[8] = 13;
	indices[9] = 14;
	indices[10] = 15;
	indices[11] = 12;
	//left plane
	indices[12] = 4;
	indices[13] = 5;
	indices[14] = 2;
	indices[15] = 4;
	indices[16] = 2;
	indices[17] = 0;
	//right plane
	indices[18] = 1;
	indices[19] = 8;
	indices[20] = 9;
	indices[21] = 1;
	indices[22] = 3;
	indices[23] = 8;
	//top plane
	indices[24] = 11;
	indices[25] = 0;
	indices[26] = 1;
	indices[27] = 11;
	indices[28] = 1;
	indices[29] = 10;
	//bottom plane
	indices[30] = 2;
	indices[31] = 6;
	indices[32] = 7;
	indices[33] = 2;
	indices[34] = 7;
	indices[35] = 3;


	D3D11_BUFFER_DESC vertexBufferDesc = { sizeof(VertexType_Texture) * vertexCount, D3D11_USAGE_DEFAULT, D3D11_BIND_VERTEX_BUFFER, 0, 0, 0 };
	vertexData = { vertices, 0 , 0 };
	device->CreateBuffer(&vertexBufferDesc, &vertexData, &vertexBuffer);

	D3D11_BUFFER_DESC indexBufferDesc = { sizeof(unsigned long) * indexCount, D3D11_USAGE_DEFAULT, D3D11_BIND_INDEX_BUFFER, 0, 0, 0 };
	indexData = { indices, 0, 0 };
	device->CreateBuffer(&indexBufferDesc, &indexData, &indexBuffer);

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete[] vertices;
	vertices = 0;
	delete[] indices;
	indices = 0;
}

void Skybox::sendData(ID3D11DeviceContext* deviceContext, D3D_PRIMITIVE_TOPOLOGY top)
{
	unsigned int stride;
	unsigned int offset;

	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType_Texture);
	offset = 0;

	deviceContext->IASetVertexBuffers(0, 1, &vertexBuffer, &stride, &offset);
	deviceContext->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
	deviceContext->IASetPrimitiveTopology(top);
}

