// Tessellation pixel shader
Texture2D grassTexture : register(t0);
Texture2D rockTexture : register(t1);
Texture2D depthMapTexture : register(t2);
SamplerState sampler0 : register(s0);
SamplerState shadowSampler : register(s1);

cbuffer LightBuffer : register(b0)
{
	float4 ambient;
	float4 diffuse;
	float3 direction;
	float padding;
};

struct InputType
{
    float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float4 lightViewPos : TEXCOORD1;
	float height : BLENDWEIGHT;
};

float4 calculateLighting(float3 lightDirection, float3 normal, float4 diffuse)
{
	float intensity = saturate(dot(normal, lightDirection));
	float4 colour = saturate(diffuse * intensity);
	return colour;
}

float4 main(InputType input) : SV_TARGET
{
	float depthValue;
	float lightDepthValue;
	float shadowMapBias = 0.001f;
	float4 colour = float4(0.f, 0.f, 0.f, 1.f);
	float4 grass = grassTexture.Sample(sampler0, input.tex * 10);
	float4 rock = rockTexture.Sample(sampler0, input.tex * 10);
	float4 textureColour = lerp(grass, rock, clamp(input.height * 2, 0, 1));

	float2 pTexCoord = input.lightViewPos.xy / input.lightViewPos.w;
	pTexCoord *= float2(0.5, -0.5);
	pTexCoord += float2(0.5f, 0.5f);

	if (pTexCoord.x < 0.f || pTexCoord.x > 1.f || pTexCoord.y < 0.f || pTexCoord.y > 1.f)
	{
		return textureColour;
	}

	depthValue = depthMapTexture.Sample(shadowSampler, pTexCoord).r;

	lightDepthValue = input.lightViewPos.z / input.lightViewPos.w;
	lightDepthValue -= shadowMapBias;

	if (lightDepthValue < depthValue)
	{
		colour = calculateLighting(-direction, input.normal, diffuse);
	}

	colour = saturate(colour + ambient);

	return colour * textureColour;




	/*
	float4 grass = grassTexture.Sample(sampler0, input.tex * 10);
	float4 rock = rockTexture.Sample(sampler0, input.tex * 10);
	//return rock;
    return lerp(grass, rock, input.height * 4);
	*/
}