#include "InteriorMappingShader.h"
#include <regex>
#include <Light.h>


InteriorMappingShader::InteriorMappingShader(ID3D11Device* device, HWND hwnd) : BaseShader(device, hwnd)
{
	initShader(L"interiorMapping_vs.cso", L"interiorMapping_ps.cso");
}


InteriorMappingShader::~InteriorMappingShader()
{
	if (sampleState)
	{
		sampleState->Release();
		sampleState = 0;
	}

	if (exteriorSampleState)
	{
		exteriorSampleState->Release();
		exteriorSampleState = 0;
	}

	if (sampleStateShadow)
	{
		sampleStateShadow->Release();
		sampleStateShadow = 0;
	}

	if (matrixBuffer)
	{
		matrixBuffer->Release();
		matrixBuffer = 0;
	}

	if (layout)
	{
		layout->Release();
		layout = 0;
	}

	if (interiorBuffer)
	{
		interiorBuffer->Release();
		interiorBuffer = 0;
	}

	if (buildingBuffer)
	{
		buildingBuffer->Release();
		buildingBuffer = 0;
	}

	if (lightBuffer)
	{
		lightBuffer->Release();
		lightBuffer = 0;
	}

	BaseShader::~BaseShader();
}

void InteriorMappingShader::initShader(WCHAR* vsFilename, WCHAR* psFilename)
{
	D3D11_BUFFER_DESC matrixBufferDesc;
	D3D11_BUFFER_DESC interiorBufferDesc;
	D3D11_BUFFER_DESC buildingBufferDesc;
	D3D11_BUFFER_DESC lightBufferDesc;
	D3D11_SAMPLER_DESC samplerDesc;

	// Load (+ compile) shader files
	loadVertexShader(vsFilename);
	loadPixelShader(psFilename);

	// Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	renderer->CreateBuffer(&matrixBufferDesc, NULL, &matrixBuffer);

	// Setup the description of the interior constant buffer that is in the pixel shader.
	interiorBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	interiorBufferDesc.ByteWidth = sizeof(InteriorBufferType);
	interiorBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	interiorBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	interiorBufferDesc.MiscFlags = 0;
	interiorBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	renderer->CreateBuffer(&interiorBufferDesc, NULL, &interiorBuffer);

	// Setup the description of the interior constant buffer that is in the vertex and pixel shaders.
	buildingBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	buildingBufferDesc.ByteWidth = sizeof(BuildingBufferType);
	buildingBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	buildingBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	buildingBufferDesc.MiscFlags = 0;
	buildingBufferDesc.StructureByteStride = 0;

	// Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
	renderer->CreateBuffer(&buildingBufferDesc, NULL, &buildingBuffer);

	//setup the light buffer
	lightBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	lightBufferDesc.ByteWidth = sizeof(LightBufferType);
	lightBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	lightBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	lightBufferDesc.MiscFlags = 0;
	lightBufferDesc.StructureByteStride = 0;

	//create the buffer pointer
	renderer->CreateBuffer(&lightBufferDesc, NULL, &lightBuffer);

	// Create a texture sampler state description.
	samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	// Create the texture sampler state.
	renderer->CreateSamplerState(&samplerDesc, &sampleState);

	//samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
	renderer->CreateSamplerState(&samplerDesc, &exteriorSampleState);


	// Sampler for shadow map sampling.
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
	samplerDesc.BorderColor[0] = 1.0f;
	samplerDesc.BorderColor[1] = 1.0f;
	samplerDesc.BorderColor[2] = 1.0f;
	samplerDesc.BorderColor[3] = 1.0f;
	renderer->CreateSamplerState(&samplerDesc, &sampleStateShadow);
}

void InteriorMappingShader::setShaderParameters(
	ID3D11DeviceContext* deviceContext,
	const XMMATRIX &worldMatrix,
	const XMMATRIX &viewMatrix,
	const XMMATRIX &projectionMatrix,
	const XMFLOAT3 cameraPos,
	const int numFloors,
	const int numRoomsX,
	const int numRoomsZ,
	const float buildingHeight,
	const float buildingWidth,
	const float buildingDepth,
	Light* light,
	ID3D11ShaderResourceView* depthMap)
{
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	MatrixBufferType* dataPtr;
	InteriorBufferType* interiorPtr;
	BuildingBufferType* buildingPtr;
	LightBufferType* lightPtr;
	XMMATRIX tworld, tview, tproj;


	// Transpose the matrices to prepare them for the shader.
	tworld = XMMatrixTranspose(worldMatrix);
	tview = XMMatrixTranspose(viewMatrix);
	tproj = XMMatrixTranspose(projectionMatrix);
	XMMATRIX tLightViewMatrix = XMMatrixTranspose(light->getViewMatrix());
	XMMATRIX tLightProjectionMatrix = XMMatrixTranspose(light->getOrthoMatrix());


	// Send matrix data
	result = deviceContext->Map(matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	dataPtr = (MatrixBufferType*)mappedResource.pData;
	dataPtr->world = tworld;// worldMatrix;
	dataPtr->view = tview;
	dataPtr->projection = tproj;
	dataPtr->lightView = tLightViewMatrix;
	dataPtr->lightProjection = tLightProjectionMatrix;
	deviceContext->Unmap(matrixBuffer, 0);
	deviceContext->VSSetConstantBuffers(0, 1, &matrixBuffer);

	// send the interior data
	result = deviceContext->Map(interiorBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	interiorPtr = (InteriorBufferType*)mappedResource.pData;
	interiorPtr->cameraPos = cameraPos;
	interiorPtr->padding = 0.f;
	deviceContext->Unmap(interiorBuffer, 0);
	deviceContext->PSSetConstantBuffers(0, 1, &interiorBuffer);

	// send the building data
	result = deviceContext->Map(buildingBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	buildingPtr = (BuildingBufferType*)mappedResource.pData;
	buildingPtr->numFloors = numFloors;
	buildingPtr->numRoomsX = numRoomsX;
	buildingPtr->numRoomsZ = numRoomsZ;
	buildingPtr->paddingA = 0;
	buildingPtr->buildingHeight = buildingHeight;
	buildingPtr->buildingWidth = buildingWidth;
	buildingPtr->buildingDepth = buildingDepth;
	buildingPtr->paddingB = 0.f;
	deviceContext->Unmap(buildingBuffer, 0);
	deviceContext->VSSetConstantBuffers(1, 1, &buildingBuffer);
	deviceContext->PSSetConstantBuffers(1, 1, &buildingBuffer);

	//send light data
	result = deviceContext->Map(lightBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	lightPtr = (LightBufferType*)mappedResource.pData;
	lightPtr->diffuseColor = light->getDiffuseColour();
	lightPtr->lightColor = light->getAmbientColour();
	lightPtr->lightDirection = light->getDirection();
	lightPtr->specularColour = light->getSpecularColour();
	lightPtr->specularPower = light->getSpecularPower();
	deviceContext->Unmap(lightBuffer, 0);
	deviceContext->PSSetConstantBuffers(2, 1, &lightBuffer);

	// Set shader texture and sampler resource in the pixel shader.
	deviceContext->PSSetShaderResources(0, 1, &ceilingTexture);
	deviceContext->PSSetShaderResources(1, 1, &floorTexture);
	deviceContext->PSSetShaderResources(2, 1, &wallTexture);
	deviceContext->PSSetShaderResources(3, 1, &exteriorTexture);
	deviceContext->PSSetShaderResources(4, 1, &environmentCubeMap);
	deviceContext->PSSetShaderResources(5, 1, &depthMap);
	deviceContext->PSSetSamplers(0, 1, &sampleState);
	deviceContext->PSSetSamplers(1, 1, &exteriorSampleState);
	deviceContext->PSSetSamplers(2, 1, &sampleStateShadow);
}