#pragma once
#include "DepthShader.h"

//variation of the normal depthGeometry Shader to use a seperate pixel shader in order to have more practical results
class DepthGeometryShaderPP : public DepthShader
{
private:
	struct BuildingBufferType
	{
		int numFloors;
		int numRoomsX;
		int numRoomsZ;
		int paddingA;
		float buildingHeight;
		float buildingWidth;
		float buildingDepth;
		float paddingB;
	};

public:
	DepthGeometryShaderPP(ID3D11Device* device, HWND hwnd);
	~DepthGeometryShaderPP();

	void setShaderParameters(
		ID3D11DeviceContext* deviceContext,
		const XMMATRIX& world,
		const XMMATRIX& view,
		const XMMATRIX& projection,
		const int numFloors,
		const int numRoomsX,
		const int numRoomsZ,
		const float buildingHeight,
		const float buildingWidth,
		const float buildingDepth);
private:
	void initShader(WCHAR*, WCHAR*, WCHAR*);

private:
	ID3D11Buffer* buildingBuffer;
};
