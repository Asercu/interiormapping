#pragma once

#include "DXF.h"

using namespace std;
using namespace DirectX;

//variation of the depthmap for use in the DOF post processing
class DepthShaderPP : public BaseShader
{
public:

	DepthShaderPP(ID3D11Device* device, HWND hwnd);
	~DepthShaderPP();

	void setShaderParameters(ID3D11DeviceContext* deviceContext, const XMMATRIX &world, const XMMATRIX &view, const XMMATRIX &projection);

protected:
	void initShader(WCHAR*, WCHAR*);

	ID3D11Buffer* matrixBuffer;
};
