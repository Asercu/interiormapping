#pragma once

#include "DXF.h"

using namespace std;
using namespace DirectX;

//modified version of the tesselateddepthshader to render out more practical results
class TessellatedDepthShaderPP : public BaseShader
{
private:
	struct TessellationBufferType
	{
		int tesselationFactor;
		float height;
		XMFLOAT2 padding;
	};

public:
	TessellatedDepthShaderPP(ID3D11Device* device, HWND hwnd);
	~TessellatedDepthShaderPP();

	void setShaderParameters(
		ID3D11DeviceContext* deviceContext,
		const XMMATRIX &world,
		const XMMATRIX &view,
		const XMMATRIX &projection,
		ID3D11ShaderResourceView* heightMap,
		float maxHeight,
		int tessellationFactor);

private:
	void initShader(WCHAR* vsFilename, WCHAR* psFilename);
	void initShader(WCHAR* vsFilename, WCHAR* hsFilename, WCHAR* dsFilename, WCHAR* psFilename);

private:
	ID3D11Buffer* matrixBuffer;
	ID3D11SamplerState* sampleState;
	ID3D11Buffer* tessellationBuffer;
};
