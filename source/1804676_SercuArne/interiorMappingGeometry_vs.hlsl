//interior mapping vertex shader for use with the geometry stage.
//input as a standard vertex with position, texcoord and normal, but we only need the worldposition.
//this shader will cull the unnecessary data

cbuffer MatrixBuffer : register(b0)
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
	matrix lightViewMatrix;
	matrix lightProjectionMatrix;
};

struct InputType
{
	float3 position : POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
};

struct OutputType
{
	float4 position : SV_POSITION;
};

OutputType main(InputType input)
{
	OutputType output;
	output.position = float4(input.position, 1);
	return output;
}