//applies Deapth of field along the horizontal axis

Texture2D shaderTexture : register(t0);
Texture2D depthMap : register(t1);
SamplerState SampleType : register(s0);

cbuffer ScreenSizeBuffer : register(b0)
{
    float screenWidth;
	float screenHeight;
	float blurIntensity;
	float dofSensitivity;
};

struct InputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
};

float4 main(InputType input) : SV_TARGET
{
	float4 colour;
	colour = shaderTexture.Sample(SampleType, input.tex);
	//create an array with the different degrees of blur, max number of neighbours is clamped at 10 in the application
	float4 blurredvalues[11];
	blurredvalues[0] = colour; //index 0 contains the original non blurred color

	float texelSize = 1.0f / screenWidth;
	//populate the rest of the array as far as necessary
	for (int i = 1; i <= blurIntensity; i++)
	{
		blurredvalues[i] = blurredvalues[i - 1];
		blurredvalues[i] += shaderTexture.Sample(SampleType, input.tex + float2(-texelSize * i, 0.0f));
		blurredvalues[i] += shaderTexture.Sample(SampleType, input.tex + float2(texelSize * i, 0.0f));
	}

	//get the depthmap values for this pixel and the center pixel
	float centerDepth = depthMap.Sample(SampleType, float2(0.5f, 0.5f)).x;
	float pixelDepth = depthMap.Sample(SampleType, input.tex).x;
	float depthDiff = abs(centerDepth - pixelDepth);
	//if the difference in depth is small enough, no blurring accurs
	if (depthDiff < dofSensitivity)
		return blurredvalues[0];

	//the bigger the difference, the stronger the blur
	int index = clamp(ceil(((depthDiff - dofSensitivity) / (1.0f - dofSensitivity)) * 2 * blurIntensity), 0, blurIntensity);
	colour = blurredvalues[index] / (2 * index + 1);
	colour.a = 1.f;
	return colour;
}
