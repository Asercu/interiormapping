// Tessellation domain shader
// After tessellation the domain shader processes the all the vertices

Texture2D texture0 : register(t0);
SamplerState sampler0 : register(s0);

cbuffer MatrixBuffer : register(b0)
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
	matrix lightViewMatrix;
	matrix lightProjectionMatrix;
};

cbuffer tesselationBuffer : register(b1)
{
	int tesselationFactor;
	float maxHeight;
	float2 padding;
}

struct ConstantOutputType
{
    float edges[4] : SV_TessFactor;
    float inside[2] : SV_InsideTessFactor;
};

struct InputType
{
    float3 position : POSITION;
	float2 tex : TEXCOORD0;
};

struct OutputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
	float3 normal : NORMAL;
	float4 lightViewPos : TEXCOORD1;
	float height : BLENDWEIGHT;
};

[domain("quad")]
OutputType main(ConstantOutputType input, float2 uvwCoord : SV_DomainLocation, const OutputPatch<InputType, 4> patch)
{
    float3 vertexPosition;
    OutputType output;
 
    // Determine the position of the new vertex.
	// Invert the y and Z components of uvwCoord as these coords are generated in UV space and therefore y is positive downward.
	// Alternatively you can set the output topology of the hull shader to cw instead of ccw (or vice versa).
	//vertexPosition = uvwCoord.x * patch[0].position + -uvwCoord.y * patch[1].position + -uvwCoord.z * patch[2].position;
	float3 v1 = lerp(patch[0].position, patch[1].position, uvwCoord.y);
	float3 v2 = lerp(patch[3].position, patch[2].position, uvwCoord.y);
	vertexPosition = lerp(v1, v2, uvwCoord.x);

	//uv coordinates
	float2 uv1 = lerp(patch[0].tex, patch[1].tex, uvwCoord.y);
	float2 uv2 = lerp(patch[3].tex, patch[2].tex, uvwCoord.y);
	float2 texCoord = lerp(uv1, uv2, uvwCoord.x);

	float4 height = texture0.SampleLevel(sampler0, texCoord, 0);
	vertexPosition.z = -height.x * maxHeight;

	output.height = height.x;

	output.tex = texCoord;

    // Calculate the position of the new vertex against the world, view, and projection matrices.
    output.position = mul(float4(vertexPosition, 1.0f), worldMatrix);
    output.position = mul(output.position, viewMatrix);
    output.position = mul(output.position, projectionMatrix);

	// Calculate the position of the vertice as viewed by the light source.
	output.lightViewPos = mul(float4(vertexPosition, 1.0f), worldMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightViewMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightProjectionMatrix);


	//normal
	//sample the pixels above, below, left and right of the current pixel in the heightmap
	//take the crossproduct of these the vectors connecting the oposite points and use that to approximate the normal of the current vertex
	float heightabove = texture0.SampleLevel(sampler0, texCoord + float2(0, -1.0f / tesselationFactor), 0).x * maxHeight;
	float heightbelow = texture0.SampleLevel(sampler0, texCoord + float2(0, 1.0f / tesselationFactor), 0).x * maxHeight;
	float heightleft = texture0.SampleLevel(sampler0, texCoord + float2(-1.0f / tesselationFactor, 0), 0).x * maxHeight;
	float heightright = texture0.SampleLevel(sampler0, texCoord + float2(1.0f / tesselationFactor, 0), 0).x * maxHeight;

	float3 diag1 = float3(0, heightabove - heightbelow, 1);
	float3 diag2 = float3(1, heightright - heightleft, 0);
	output.normal = normalize(cross(diag1, diag2));
    return output;
}

