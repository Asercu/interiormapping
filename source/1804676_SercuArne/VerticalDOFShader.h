#pragma once
#include "DXF.h"

using namespace std;
using namespace DirectX;

//post processing shader that applies vertical Depth of field
class VerticalDOFShader : public BaseShader
{
private:
	struct ScreenSizeBufferType
	{
		float screenWidth;
		float screenHeight;
		float blurIntensity;
		float DOFSensitivity;
	};

public:
	VerticalDOFShader(ID3D11Device* device, HWND hwnd);
	~VerticalDOFShader();

	void setShadeParameters(
		ID3D11DeviceContext* deviceContext,
		const XMMATRIX &world,
		const XMMATRIX &view,
		const XMMATRIX &proj,
		ID3D11ShaderResourceView* texture,
		ID3D11ShaderResourceView* depthMap,
		float screenWidth,
		float screenHeight,
		float DOFSensitivity,
		float blurIntensity);

private:
	void initShader(WCHAR*, WCHAR*);

	ID3D11Buffer* matrixBuffer;
	ID3D11SamplerState* sampleState;
	ID3D11Buffer* screenSizeBuffer;
};