//generates the geometry for the building with an outputtype compatible with the depth shaders

cbuffer MatrixBuffer : register(b0)
{
    matrix worldMatrix;
    matrix viewMatrix;
    matrix projectionMatrix;
};

cbuffer BuildingBuffer : register(b1)
{
	int numFloors;
	int numRoomsX;
	int numRoomsZ;
	int paddingA;
	float buildingHeight;
	float buildingWidth;
	float buildingDepth;
	float paddingB;
}

struct InputType
{
	float4 position : POSITION;
};

struct OutputType
{
	float4 position : SV_POSITION;
	float4 depthPosition : TEXCOORD0;
};

OutputType bottomLeftFront(InputType input)
{
	OutputType output;
	output.position = mul(float4(input.position.x + 0.005f, input.position.y + 0.005f, input.position.z + 0.005f, input.position.w), worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);
	output.depthPosition = output.position;
	return output;
}

OutputType bottomRightFront(InputType input)
{
	OutputType output;
	output.position = mul(float4(input.position.x + 2.0f * buildingWidth * 0.99f, input.position.y + 0.005f, input.position.z + 0.005f, input.position.w), worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);
	output.depthPosition = output.position;
	return output;
}

OutputType topRightFront(InputType input)
{
	OutputType output;
	output.position = mul(float4(input.position.x + 2.0f * buildingWidth * 0.99f, input.position.y + 2.0f * buildingHeight * 0.99f, input.position.z + 0.005f, input.position.w), worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);
	output.depthPosition = output.position;
	return output;
}

OutputType topLeftFront(InputType input)
{
	OutputType output;
	output.position = mul(float4(input.position.x + 0.0005f, input.position.y + 2.0f * buildingHeight * 0.99f, input.position.z + 0.005f, input.position.w), worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);
	output.depthPosition = output.position;
	return output;
}

OutputType bottomLeftBack(InputType input)
{
	OutputType output;;
	output.position = mul(float4(input.position.x + 0.005f, input.position.y + 0.005f, input.position.z + 2.0f * buildingDepth * 0.99f, input.position.w), worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);
	output.depthPosition = output.position;
	return output;
}

OutputType bottomRightBack(InputType input)
{
	OutputType output;
	output.position = mul(float4(input.position.x + 2.0f * buildingWidth * 0.99f, input.position.y + 0.005f, input.position.z + 2.0f * buildingDepth * 0.99f, input.position.w), worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);
	output.depthPosition = output.position;
	return output;
}

OutputType topRightBack(InputType input)
{
	OutputType output;
	output.position = mul(float4(input.position.x + 2.0f * buildingWidth * 0.99f, input.position.y + 2.0f * buildingHeight * 0.99f, input.position.z + 2.0f * buildingDepth * 0.99f, input.position.w), worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);
	output.depthPosition = output.position;
	return output;
}

OutputType topLeftBack(InputType input)
{
	OutputType output;
	output.position = mul(float4(input.position.x + 0.005f, input.position.y + 2.0f * buildingHeight * 0.99f, input.position.z + 2.0f * buildingDepth * 0.99f, input.position.w), worldMatrix);
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);
	output.depthPosition = output.position;
	return output;
}

[maxvertexcount(36)]
void main(point InputType input[1], inout TriangleStream<OutputType> triStream)
{
	OutputType output;
	//front
	triStream.Append(bottomLeftFront(input[0]));
	triStream.Append(topRightFront(input[0]));
	triStream.Append(topLeftFront(input[0]));
	triStream.RestartStrip();
	triStream.Append(bottomLeftFront(input[0]));
	triStream.Append(bottomRightFront(input[0]));
	triStream.Append(topRightFront(input[0]));
	triStream.RestartStrip();
	//right
	triStream.Append(bottomRightFront(input[0]));
	triStream.Append(bottomRightBack(input[0]));
	triStream.Append(topRightBack(input[0]));
	triStream.RestartStrip();
	triStream.Append(bottomRightFront(input[0]));
	triStream.Append(topRightBack(input[0]));
	triStream.Append(topRightFront(input[0]));
	triStream.RestartStrip();
	//back
	triStream.Append(bottomLeftBack(input[0]));
	triStream.Append(topLeftBack(input[0]));
	triStream.Append(bottomRightBack(input[0]));
	triStream.RestartStrip();
	triStream.Append(topLeftBack(input[0]));
	triStream.Append(topRightBack(input[0]));
	triStream.Append(bottomRightBack(input[0]));
	triStream.RestartStrip();
	//left
	triStream.Append(bottomLeftFront(input[0]));
	triStream.Append(topLeftBack(input[0]));
	triStream.Append(bottomLeftBack(input[0]));
	triStream.RestartStrip();
	triStream.Append(bottomLeftFront(input[0]));
	triStream.Append(topLeftFront(input[0]));
	triStream.Append(topLeftBack(input[0]));
	triStream.RestartStrip();
	//top
	triStream.Append(topRightFront(input[0]));
	triStream.Append(topLeftBack(input[0]));
	triStream.Append(topLeftFront(input[0]));
	triStream.RestartStrip();
	triStream.Append(topRightFront(input[0]));
	triStream.Append(topRightBack(input[0]));
	triStream.Append(topLeftBack(input[0]));
	triStream.RestartStrip();
	//bottom
	triStream.Append(bottomRightFront(input[0]));
	triStream.Append(bottomLeftFront(input[0]));
	triStream.Append(bottomLeftBack(input[0]));
	triStream.RestartStrip();
	triStream.Append(bottomRightFront(input[0]));
	triStream.Append(bottomLeftBack(input[0]));
	triStream.Append(bottomRightBack(input[0]));
	triStream.RestartStrip();
}