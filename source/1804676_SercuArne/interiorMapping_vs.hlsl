//vertex shader for interior mapping on an existing cube.
//transform the vertices as normal with world, view and projection matrices
//adjust the texture coordinates so it fits with the number of rooms and floors

cbuffer MatrixBuffer : register(b0)
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
	matrix lightViewMatrix;
	matrix lightProjectionMatrix;
};

cbuffer BuildingBuffer : register(b1)
{
	int numFloors;
	int numRoomsX;
	int numRoomsZ;
	int paddingA;
	float buildingHeight;
	float buildingWidth;
	float buildingDepth;
	float paddingB;
}

struct InputType
{
	float4 position : POSITION;
	float2 tex : TEXCOORD0;
    float3 normal : NORMAL;
};

struct OutputType
{
	float4 position : SV_POSITION;
	float2 tex : TEXCOORD0;
    float3 normal : NORMAL;
	float4 worldPosition : TEXCOORD1;
	float4 lightViewPos : TEXCOORD2;
};

OutputType main(InputType input)
{
	OutputType output;

	// Calculate the position of the vertex against the world, view, and projection matrices.
	output.position = mul(input.position, worldMatrix);
	output.worldPosition = output.position;
	output.position = mul(output.position, viewMatrix);
	output.position = mul(output.position, projectionMatrix);

	output.lightViewPos = mul(input.position, worldMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightViewMatrix);
	output.lightViewPos = mul(output.lightViewPos, lightProjectionMatrix);


	// Store the texture coordinates for the pixel shader.
	//use normal to find out what plane of the cube this vector is on
	if (abs(input.normal.x) > abs(input.normal.y) && abs(input.normal.x) > abs(input.normal.z))
	{
		//YZ plane
		output.tex = float2(input.tex.x * numRoomsZ, input.tex.y * numFloors);
	}
	else if (abs(input.normal.y) > abs(input.normal.z) && abs(input.normal.y) > abs(input.normal.x))
	{
		//XZ plane
		output.tex = float2(0, 0);
	}
	else
	{
		//XY plane
		output.tex = float2(input.tex.x * numRoomsX, input.tex.y * numFloors);
	}

    output.normal = input.normal;

	return output;
}