#include "App1.h"
#include "TesselatedDepthShader.h"

App1::App1()
{

}

void App1::init(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight, Input *in, bool VSYNC, bool FULL_SCREEN)
{
	// Call super/parent init function (required!)
	BaseApplication::init(hinstance, hwnd, screenWidth, screenHeight, in, VSYNC, FULL_SCREEN);

	// Load all textures in the manager
	textureMgr->loadTexture("ceiling", L"res/ceiling.png ");
	textureMgr->loadTexture("floor", L"res/floor.png");
	textureMgr->loadTexture("wall", L"res/wallHalf.png");
	textureMgr->loadTexture("exterior", L"res/exteriorBrick.png");
	textureMgr->loadTexture("ground", L"res/ground.png");
	textureMgr->loadTexture("sky", L"res/skyBox.dds");
	textureMgr->loadTexture("height", L"res/height2.png");
	textureMgr->loadTexture("rock", L"res/rock.png");
	
	//create shader objects
	textureShader = new TextureShader(renderer->getDevice(), hwnd);
	interiorMappingShader = new InteriorMappingShader(renderer->getDevice(), hwnd);
	interiorMappingGeometryShader = new InteriorMappingGeometryShader(renderer->getDevice(), hwnd);
	shadowShader = new ShadowShader(renderer->getDevice(), hwnd);
	depthShader = new DepthShader(renderer->getDevice(), hwnd);
	depthGeometryShader = new DepthGeometryShader(renderer->getDevice(), hwnd);
	depthShaderPP = new DepthShaderPP(renderer->getDevice(), hwnd);
	depthGeometryShaderPP = new DepthGeometryShaderPP(renderer->getDevice(), hwnd);
	skyboxShader = new SkyBoxShader(renderer->getDevice(), hwnd);
	horizontalDOFShader = new HorizontalDOFShader(renderer->getDevice(), hwnd);
	verticalDOFShader = new VerticalDOFShader(renderer->getDevice(), hwnd);
	tessellationShader = new TessellationShader(renderer->getDevice(), hwnd);
	tessellatedDepthShader = new TessellatedDepthShader(renderer->getDevice(), hwnd);
	tessellatedDepthShaderPP = new TessellatedDepthShaderPP(renderer->getDevice(), hwnd);

	//set the textures for both interior mapping shaders
	interiorMappingShader->SetCeilingTexture(textureMgr->getTexture("ceiling"));
	interiorMappingShader->SetFloorTexture(textureMgr->getTexture("floor"));
	interiorMappingShader->SetWallTexture(textureMgr->getTexture("wall"));
	interiorMappingShader->SetExteriorTexture(textureMgr->getTexture("exterior"));
	interiorMappingShader->SetEnvironmentMap(textureMgr->getTexture("sky"));
	interiorMappingGeometryShader->SetCeilingTexture(textureMgr->getTexture("ceiling"));
	interiorMappingGeometryShader->SetFloorTexture(textureMgr->getTexture("floor"));
	interiorMappingGeometryShader->SetWallTexture(textureMgr->getTexture("wall"));
	interiorMappingGeometryShader->SetExteriorTexture(textureMgr->getTexture("exterior"));
	interiorMappingGeometryShader->SetEnvironmentMap(textureMgr->getTexture("sky"));

	//create rendertextures
	int shadowMapWidth = 2048;
	int shadowMapHeight = 2048;
	int sceneWidth = 150;
	int sceneHeight = 150;
	shadowMap = new RenderTexture(renderer->getDevice(), shadowMapWidth, shadowMapHeight, SCREEN_NEAR, SCREEN_DEPTH);
	depthMap = new RenderTexture(renderer->getDevice(), screenWidth, screenHeight, SCREEN_NEAR, 100.f);
	horizontalDOFTexture = new RenderTexture(renderer->getDevice(), screenWidth, screenHeight, SCREEN_NEAR, SCREEN_DEPTH);
	verticalDOFTexture = new RenderTexture(renderer->getDevice(), screenWidth, screenHeight, SCREEN_NEAR, SCREEN_DEPTH);
	MainRenderTexture = new RenderTexture(renderer->getDevice(), screenWidth, screenHeight, SCREEN_NEAR, SCREEN_DEPTH);


	//create scene objects
	cubeBuilding = new CubeMesh(renderer->getDevice(), renderer->getDeviceContext(), 1);
	pointBuilding = new MeshPoint(renderer->getDevice(), renderer->getDeviceContext(), {0, 0, 0});
	sky = new Skybox(renderer->getDevice(), renderer->getDeviceContext());
	orthoMesh = new OrthoMesh(renderer->getDevice(), renderer->getDeviceContext(), screenWidth, screenHeight);
	quad = new Quad(renderer->getDevice(), renderer->getDeviceContext());
	light = new Light();
	light->setAmbientColour(.3f, .3f, .3f, 1.f);
	light->setDiffuseColour(1.f, 1.f, 1.f, 1.f);
	light->setDirection(0.333f, -.667f, .667f);
	light->setSpecularPower(50.f);
	light->setSpecularColour(.99f, .36f, 0.f, 1.f);
	light->setPosition(-10.f, 50.f, 20.f);
	light->generateOrthoMatrix((float)sceneWidth, (float)sceneHeight, .1f, 100.f);

	//move camera up a bit 
	camera->setPosition(0.f, 2.f, -10.f);
}


App1::~App1()
{
	// Run base application deconstructor
	BaseApplication::~BaseApplication();

	// Release the shaders
	if (textureShader)
	{
		delete textureShader;
		textureShader = 0;
	}
	if (interiorMappingShader)
	{
		delete interiorMappingShader;
		interiorMappingShader = 0;
	}
	if (interiorMappingGeometryShader)
	{
		delete interiorMappingGeometryShader;
		interiorMappingGeometryShader = 0;
	}
	if (shadowShader)
	{
		delete shadowShader;
		shadowShader = 0;
	}
	if (depthShader)
	{
		delete depthShader;
		depthShader = 0;
	}
	if (depthGeometryShader)
	{
		delete depthGeometryShader;
		depthGeometryShader = 0;
	}
	if (depthShaderPP)
	{
		delete depthShaderPP;
		depthShaderPP = 0;
	}
	if (depthGeometryShaderPP)
	{
		delete depthGeometryShaderPP;
		depthGeometryShaderPP = 0;
	}
	if (skyboxShader)
	{
		delete skyboxShader;
		skyboxShader = 0;
	}
	if (horizontalDOFShader)
	{
		delete horizontalDOFShader;
		horizontalDOFShader = 0;
	}
	if (verticalDOFShader)
	{
		delete verticalDOFShader;
		verticalDOFShader = 0;
	}
	if (tessellationShader)
	{
		delete tessellationShader;
		tessellationShader = 0;
	}
	if (tessellatedDepthShader)
	{
		delete tessellatedDepthShader;
		tessellatedDepthShader = 0;
	}
	if (tessellatedDepthShaderPP)
	{
		delete tessellatedDepthShaderPP;
		tessellatedDepthShaderPP = 0;
	}

	//scene objects
	if (cubeBuilding)
	{
		delete cubeBuilding;
		cubeBuilding = 0;
	}
	if (pointBuilding)
	{
		delete pointBuilding;
		pointBuilding = 0;
	}
	if (quad)
	{
		delete quad;
		quad = 0;
	}
	if (sky)
	{
		delete sky;
		sky = 0;
	}
	if (light)
	{
		delete light;
		light = 0;
	}
	if (orthoMesh)
	{
		delete orthoMesh;
		orthoMesh = 0;
	}

	//render targets
	if (shadowMap)
	{
		delete shadowMap;
		shadowMap = 0;
	}
	if (depthMap)
	{
		delete depthMap;
		depthMap = 0;
	}
	if (verticalDOFTexture)
	{
		delete verticalDOFTexture;
		verticalDOFTexture = 0;
	}
	if (horizontalDOFTexture)
	{
		delete horizontalDOFTexture;
		horizontalDOFTexture = 0;
	}
	if (MainRenderTexture)
	{
		delete MainRenderTexture;
		MainRenderTexture = 0;
	}
}

bool App1::frame()
{
	bool result;

	result = BaseApplication::frame();
	if (!result)
	{
		return false;
	}

	//clamp the ImGui variables to avoid weirdness
	if (numFloors < 1) numFloors = 1;
	if (numRoomsX < 1) numRoomsX = 1;
	if (numRoomsZ < 1) numRoomsZ = 1;
	if (blurIntensity < 0) blurIntensity = 0;
	if (blurIntensity > 10) blurIntensity = 10;

	// Render the graphics.
	result = render();
	if (!result)
	{
		return false;
	}

	return true;
}

bool App1::render()
{
	shadowDepthPass(); //render shadowmap
	mainPass(); //render actual scene
	postProcessDepthPass(); //render depthmap used in PP
	horizontalDOF(); //apply Depth of Field along th horizontal axis
	verticalDOF(); //apply Deapth of Field along the vertical axis
	finalPass(); //render final image 		
	return true;
}

//generate shadowmap
void App1::shadowDepthPass()
{
	//set the rendertarget
	shadowMap->setRenderTarget(renderer->getDeviceContext());
	shadowMap->clearRenderTarget(renderer->getDeviceContext(), 1.f, 1.f, 1.f, 1.f);

	ID3D11RasterizerState* rs;
	renderer->getDeviceContext()->RSGetState(&rs);
	D3D11_RASTERIZER_DESC* originalRSDesc = new D3D11_RASTERIZER_DESC();
	rs->GetDesc(originalRSDesc);
	D3D11_RASTERIZER_DESC* noCullRSDesc = new D3D11_RASTERIZER_DESC();
	rs->GetDesc(noCullRSDesc);
	noCullRSDesc->CullMode = D3D11_CULL_NONE;
	renderer->getDevice()->CreateRasterizerState(noCullRSDesc, &rs);
	renderer->getDeviceContext()->RSSetState(rs);

	//generate matrices
	light->generateViewMatrix();
	XMMATRIX lightViewMatrix = light->getViewMatrix();
	XMMATRIX lightProjectionMatrix = light->getOrthoMatrix();
	XMMATRIX worldMatrix = renderer->getWorldMatrix();

	//render the house
	if(!usePointMesh)
	{
		//using the cubemesh
		//the building is translated a bit because the origin is in the center of the cube, and a little bit more because the interiormapping
		//without the .005 and scaling *.99 there would be inside walls in the same plane as the outside wall, causing the algorithm to render an inside wall glued to the window
		worldMatrix = XMMatrixTranslation(1.005f, 1.f, 1.005f) * XMMatrixScaling(buildingWidth * .99f, buildingHeight * .99f, buildingDepth * .99f);
		cubeBuilding->sendData(renderer->getDeviceContext());
		depthShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, lightViewMatrix, lightProjectionMatrix);
		depthShader->render(renderer->getDeviceContext(), cubeBuilding->getIndexCount());
	}
	else
	{
		//generate the building on the gpu using the geometry shader
		pointBuilding->sendData(renderer->getDeviceContext());
		depthGeometryShader->setShaderParameters(
			renderer->getDeviceContext(),
			worldMatrix, lightViewMatrix,
			lightProjectionMatrix,
			numFloors,
			numRoomsX,
			numRoomsZ,
			buildingHeight,
			buildingWidth,
			buildingDepth);
		depthGeometryShader->render(renderer->getDeviceContext(), pointBuilding->getIndexCount());
	}

	//tessellatedground
	worldMatrix = XMMatrixTranslation(0.3f, 0.3f, 0) * XMMatrixRotationRollPitchYaw(XM_PIDIV2, 0, 0) * XMMatrixScaling(50, 1, 50);
	quad->sendData(renderer->getDeviceContext(), D3D11_PRIMITIVE_TOPOLOGY_4_CONTROL_POINT_PATCHLIST);
	tessellatedDepthShader->setShaderParameters(
		renderer->getDeviceContext(),
		worldMatrix,
		lightViewMatrix,
		lightProjectionMatrix,
		textureMgr->getTexture("height"),
		maxheight,
		tesFactor);
	tessellatedDepthShader->render(renderer->getDeviceContext(), quad->getIndexCount());

	//reset rasterizerstate
	renderer->getDevice()->CreateRasterizerState(originalRSDesc, &rs);
	renderer->getDeviceContext()->RSSetState(rs);

	//reset back buffer and viewport
	renderer->setBackBufferRenderTarget();
	renderer->resetViewport();
}

//render the scene
void App1::mainPass()
{
	MainRenderTexture->setRenderTarget(renderer->getDeviceContext());
	MainRenderTexture->clearRenderTarget(renderer->getDeviceContext(), 0, 0, 0, 1.f);


	camera->update();

	//generate matrices
	XMMATRIX worldMatrix = renderer->getWorldMatrix();
	XMMATRIX viewMatrix = camera->getViewMatrix();
	XMMATRIX projectionMatrix = renderer->getProjectionMatrix();
	worldMatrix = XMMatrixScaling(100.f, 100.f, 100.f) * XMMatrixRotationRollPitchYaw(0.f, XM_PI, 0.f); //rotate so the light direction for shadows matches the skybox

	//render skybox on far plane
	renderer->setZBuffer(false);

	sky->sendData(renderer->getDeviceContext());
	skyboxShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, viewMatrix, projectionMatrix, textureMgr->getTexture("sky"));
	skyboxShader->render(renderer->getDeviceContext(), sky->getIndexCount());

	renderer->setZBuffer(true);

	//render the house
	if (!usePointMesh)
	{
		//see ShadowDepthPass
		worldMatrix = XMMatrixTranslation(1.005f, 1.f, 1.005f) * XMMatrixScaling(buildingWidth * .99f, buildingHeight * .99f, buildingDepth * .99f);

		//cube house
		cubeBuilding->sendData(renderer->getDeviceContext());
		interiorMappingShader->setShaderParameters(
			renderer->getDeviceContext(),
			worldMatrix,
			viewMatrix,
			projectionMatrix,
			camera->getPosition(),
			numFloors,
			numRoomsX,
			numRoomsZ,
			buildingHeight,
			buildingWidth,
			buildingDepth,
			light,
			shadowMap->getShaderResourceView());
		interiorMappingShader->render(renderer->getDeviceContext(), cubeBuilding->getIndexCount());
	}
	else
	{
		//generate house on GPU
		worldMatrix = renderer->getWorldMatrix();
		pointBuilding->sendData(renderer->getDeviceContext());
		interiorMappingGeometryShader->setShaderParameters(
			renderer->getDeviceContext(),
			worldMatrix,
			viewMatrix,
			projectionMatrix,
			camera->getPosition(),
			numFloors,
			numRoomsX,
			numRoomsZ,
			buildingHeight,
			buildingWidth,
			buildingDepth,
			light,
			shadowMap->getShaderResourceView());
		interiorMappingGeometryShader->render(renderer->getDeviceContext(), pointBuilding->getIndexCount());
	}

	//tesselatedground
	worldMatrix = XMMatrixTranslation(0.3f, 0.3f, 0) * XMMatrixRotationRollPitchYaw(XM_PIDIV2, 0, 0) * XMMatrixScaling(50, 1, 50);
	quad->sendData(renderer->getDeviceContext(), D3D11_PRIMITIVE_TOPOLOGY_4_CONTROL_POINT_PATCHLIST);
	tessellationShader->setShaderParameters(
		renderer->getDeviceContext(),
		worldMatrix,
		viewMatrix,
		projectionMatrix,
		textureMgr->getTexture("height"),
		textureMgr->getTexture("ground"),
		textureMgr->getTexture("rock"),
		shadowMap->getShaderResourceView(),
		light,
		maxheight,
		tesFactor);
	tessellationShader->render(renderer->getDeviceContext(), quad->getIndexCount());


	//reset buffer and viewport
	renderer->setBackBufferRenderTarget();
	renderer->resetViewport();
}

//render depthmap for PP
void App1::postProcessDepthPass()
{
	camera->update();

	//set the rendertarget
	depthMap->setRenderTarget(renderer->getDeviceContext());
	depthMap->clearRenderTarget(renderer->getDeviceContext(), 1.f, 1.f, 1.f, 1.f);

	XMMATRIX viewMatrix = camera->getViewMatrix();
	XMMATRIX projectionMatrix =  renderer->getProjectionMatrix();
	XMMATRIX worldMatrix = renderer->getWorldMatrix();


	//basically the same as previous render functions
	if(!usePointMesh)
	{
		worldMatrix = XMMatrixTranslation(1.005f, 1.f, 1.005f) * XMMatrixScaling(buildingWidth * .99f, buildingHeight * .99f, buildingDepth * .99f);
		cubeBuilding->sendData(renderer->getDeviceContext());
		depthShaderPP->setShaderParameters(renderer->getDeviceContext(), worldMatrix, viewMatrix, projectionMatrix);
		depthShaderPP->render(renderer->getDeviceContext(), cubeBuilding->getIndexCount());
	}
	else
	{
		pointBuilding->sendData(renderer->getDeviceContext());
		depthGeometryShaderPP->setShaderParameters(
			renderer->getDeviceContext(),
			worldMatrix, viewMatrix,
			projectionMatrix,
			numFloors,
			numRoomsX,
			numRoomsZ,
			buildingHeight,
			buildingWidth,
			buildingDepth);
		depthGeometryShaderPP->render(renderer->getDeviceContext(), pointBuilding->getIndexCount());
	}

	//tesselatedground
	worldMatrix = XMMatrixTranslation(0.3f, 0.3f, 0) * XMMatrixRotationRollPitchYaw(XM_PIDIV2, 0, 0) * XMMatrixScaling(50, 1, 50);
	quad->sendData(renderer->getDeviceContext(), D3D11_PRIMITIVE_TOPOLOGY_4_CONTROL_POINT_PATCHLIST);
	tessellatedDepthShaderPP->setShaderParameters(
		renderer->getDeviceContext(),
		worldMatrix, 
		viewMatrix, 
		projectionMatrix, 
		textureMgr->getTexture("height"),
		maxheight,
		tesFactor);
	tessellatedDepthShaderPP->render(renderer->getDeviceContext(), quad->getIndexCount());
	

	//reset back buffer and viewport
	renderer->setBackBufferRenderTarget();
	renderer->resetViewport();
}

//Do first PP pass, applying depth of field effect on the horizontal axis
void App1::horizontalDOF()
{
	XMMATRIX worldMatrix, viewMatrix, orthoMatrix;
	float screenWidth = (float)depthMap->getTextureWidth();
	float screenHeight = (float)depthMap->getTextureHeight();

	horizontalDOFTexture->setRenderTarget(renderer->getDeviceContext());
	horizontalDOFTexture->clearRenderTarget(renderer->getDeviceContext(), 0, 0, 0, 1.f);

	worldMatrix = renderer->getWorldMatrix();
	viewMatrix = camera->getOrthoViewMatrix();
	orthoMatrix = horizontalDOFTexture->getOrthoMatrix();

	renderer->setZBuffer(false);
	orthoMesh->sendData(renderer->getDeviceContext());
	horizontalDOFShader->setShadeParameters(
		renderer->getDeviceContext(),
		worldMatrix,
		viewMatrix,
		orthoMatrix,
		MainRenderTexture->getShaderResourceView(),
		depthMap->getShaderResourceView(),
		screenWidth,
		screenHeight,
		DOFSensitivity,
		(float)blurIntensity);
	horizontalDOFShader->render(renderer->getDeviceContext(), orthoMesh->getIndexCount());
	renderer->setZBuffer(true);

	renderer->setBackBufferRenderTarget();
	renderer->resetViewport();
}

//Do second PP pass, applying depth of field effect on the vertical axis
void App1::verticalDOF()
{
	XMMATRIX worldMatrix, viewMatrix, orthoMatrix;
	float screenWidth = (float)depthMap->getTextureWidth();
	float screenHeight = (float)depthMap->getTextureHeight();

	verticalDOFTexture->setRenderTarget(renderer->getDeviceContext());
	verticalDOFTexture->clearRenderTarget(renderer->getDeviceContext(), 0, 0, 0, 1.f);

	worldMatrix = renderer->getWorldMatrix();
	viewMatrix = camera->getOrthoViewMatrix();
	orthoMatrix = verticalDOFTexture->getOrthoMatrix();

	renderer->setZBuffer(false);
	orthoMesh->sendData(renderer->getDeviceContext());
	verticalDOFShader->setShadeParameters(
		renderer->getDeviceContext(),
		worldMatrix,
		viewMatrix,
		orthoMatrix,
		horizontalDOFTexture->getShaderResourceView(),
		depthMap->getShaderResourceView(),
		screenWidth,
		screenHeight,
		DOFSensitivity,
		(float)blurIntensity);
	verticalDOFShader->render(renderer->getDeviceContext(), orthoMesh->getIndexCount());
	renderer->setZBuffer(true);

	renderer->setBackBufferRenderTarget();
	renderer->resetViewport();
}

//render the result to the screen
void App1::finalPass()
{
	renderer->beginScene(0, 0, 0, 1);

	renderer->setZBuffer(false);
	XMMATRIX worldMatrix = renderer->getWorldMatrix();
	XMMATRIX orthoMatrix = renderer->getOrthoMatrix();
	XMMATRIX orthoViewMatrix = camera->getOrthoViewMatrix();

	orthoMesh->sendData(renderer->getDeviceContext());
	textureShader->setShaderParameters(renderer->getDeviceContext(), worldMatrix, orthoViewMatrix, orthoMatrix, verticalDOFTexture->getShaderResourceView());
	textureShader->render(renderer->getDeviceContext(), orthoMesh->getIndexCount());
	renderer->setZBuffer(true);

	// Render GUI
	gui();

	// Present the rendered scene to the screen.
	renderer->endScene();
}

void App1::gui()
{
	// Force turn off unnecessary shader stages.
	renderer->getDeviceContext()->GSSetShader(NULL, NULL, 0);
	renderer->getDeviceContext()->HSSetShader(NULL, NULL, 0);
	renderer->getDeviceContext()->DSSetShader(NULL, NULL, 0);

	// Build UI
	ImGui::Text("FPS: %.2f", timer->getFPS());
	ImGui::Checkbox("Wireframe mode", &wireframeToggle);
	ImGui::Checkbox("generate building on GPU", &usePointMesh);
	ImGui::SliderInt("Tessellation Factor", &tesFactor, 1, 64);
	ImGui::SliderFloat("Max height mountains", &maxheight, 0.f, 100.f);
	ImGui::InputInt("floors", &numFloors, 1, 1);
	ImGui::InputInt("rooms per width", &numRoomsX, 1, 1);
	ImGui::InputInt("rooms per depth", &numRoomsZ, 1, 1);
	ImGui::SliderFloat("building height", &buildingHeight, 0.f, 10.f);
	ImGui::SliderFloat("building width", &buildingWidth, 0.f, 10.f);
	ImGui::SliderFloat("building depth", &buildingDepth, 0.f, 10.f);
	ImGui::SliderFloat("DOF treshold", &DOFSensitivity, 0.f, .4f);
	ImGui::InputInt("Blur intensity", &blurIntensity, 1, 1);

	// Render UI
	ImGui::SetNextWindowSize(ImVec2(200, 100), ImGuiSetCond_FirstUseEver);
	ImGui::Render();
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
}

